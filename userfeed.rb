require './feed.rb'
require './registry.rb'

class UserFeed < Feed
  "feed of posts from a user"
  # construct a UserFeed from a refrence
  def UserFeed.ref(nick, url)
    f = UserFeed.new("# nick = #{nick}\n# url = #{url}")
    f.last_checked = nil
    return f
  end
  def initialize(raw)
    @posts = []
    @meta = Hash.new
    @last_checked = DateTime.now
    for ln in raw.split "\n" do
      if ln.empty? or ln.start_with? "#" then
        if (m = ln.match /^#\s*([a-zA-Z_-]+)\s*=\s*(.+)$/) then
          k = m[1]
          v = m[2]
          @meta[k] = @meta[k] || []
          @meta[k] << v
        end
      else
        begin
          @posts << Post.parse(self, ln)
        rescue => e
          #print "malformed post: #{ln} #{e}"
          # malformed posts are ignored
        end
      end
    end
  end
  attr_reader :posts, :meta, :last_checked
  attr_writer :last_checked
  def get_meta_list(k) = meta[k] || []
  # get the first metadata item with the given key
  def get_meta(k) = get_meta_list(k)[0]
  def add_meta(k, v)
    if meta[k] == nil then
      meta[k] = [v]
    else
      meta[k] << v
    end
  end
  def url = get_meta "url"
  def nick = get_meta "nick"
  # our own ad-hoc extension: defines the base url for a registry that all posts to this feed will be announced at
  def registry_url = get_meta "registry"
  def registry
    u = self.registry_url
    if u then
      return Registry.new(self.registry_url)
    end
    return nil
  end
  def ipns
    self.meta["url"].each{|u|
      url = URI(u)
      if url.scheme == "ipns" then
        return url.host
      end
    }
    return nil
  end
  def refresh
    # TODO: use If-Updated-Since header?
    new_raw = fetch_a_url(self.meta["url"])
    if new_raw == nil then
      return false
    end
    new_feed = UserFeed.new new_raw
    # if a feed doesn't have a url, maintain the url it was retrived from
    if new_feed.url == nil then
      new_feed.meta["url"] = self.meta["url"]
    end
    # TODO: maybe maintain other meta fields like nickname?
    @posts = new_feed.posts
    @last_checked = new_feed.last_checked
    @meta = new_feed.meta
    return
  end
  def to_s
    s = ""
    @meta.each {|k,vs|
      vs.each {|v|
        s << "# #{k} = #{v}\n"
      }
    }
    @posts.each {|post|
      s << post.to_s
      s << "\n"
    }
    return s
  end
  def timeline
    "feed of posts from everyone this user follows"
    feeds = []
    for val in self.meta["follow"] do
      # yes, this is how we construct feeds from a url.
      # the other details will get filled in when the feed is refreshed.
      feeds << UserFeed.new("# url = "+val.split(" ")[-1])
    end
    return MultiFeed.new feeds
  end
  def lookup_user(u)
    self.meta["follow"].each{|s|
      nick, url = s.split(' ')
      if nick == u then return UserFeed.ref(nick, url) end
    }
    super u
  end
  def follows
    l = []
    self.get_meta_list("follow").each{|s|
      nick, url = s.split(' ')
      l << UserFeed.ref(nick, url)
    }
    return l
  end
end

class FileFeed < UserFeed
  "user feed that is stored in a file (possibly mounted via sshfs), and that can be editied."
  
  def initialize(filename)
    @filename = filename
    super File.read @filename
  end
  attr_reader :filename
  def new_post(body)
    # TODO: just append to the end of the file instead of doing this whole dance
    self.refresh
    @posts << Post.new(self, DateTime.now, body)
    self.flush
    return nil
  end
  def flush
    File.open(self.filename, File::RDWR) {|f|
      f.write(self.to_s)
    }
    return nil
  end
end
