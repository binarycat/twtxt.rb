require 'net/http'



# TODO: if there is a local ipfs node, use that for ipfs instead
def fetch_url(url_str)
  # ruby doesn't seem to have file-local variables
  # the proper way to do this is with an object
  proxy_tbl = Hash.new
  
  proxy_tbl['gemini'] = ['https://portal.mozz.us/gemini/$1?raw=1']
  proxy_tbl['ipfs'] = ['https://gateway.ipfs.io/ipfs/$1', 'https://ipfs.io/ipfs/$1']
  proxy_tbl['ipns'] = [
    'https://hardbin.com/ipns/$1',
    'https://gateway.ipfs.io/ipns/$1',
    'https://ipfs.io/ipns/$1',
  ]

  url = URI(url_str)
  proxies = proxy_tbl[url.scheme]
  if proxies != nil then
    return fetch_a_url(proxies.map{|proxy|
      proxy.gsub("$1", url_str.gsub(/^[a-z]+:\/\//, ""))})
  else
    ## TODO: Accept: text/plain
    return Net::HTTP.get(url).force_encoding('utf-8')
  end
end

def fetch_a_url(url_list)
  body = nil
  url_list.each {|url_str|
    begin
      body = fetch_url(url_str)
    rescue => e
      body = nil
    end
    if body != nil then
      return body
    end
  }
  return nil
end
