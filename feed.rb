require 'date'
require './fetch.rb'

class Post
  include Comparable
  def initialize(feed, timestamp, body)
    @timestamp = timestamp
    @body = body
    @feed = feed
  end
  attr_reader :timestamp, :body, :feed
  # convert to string in twtxt format
  def to_s = timestamp.to_s+"\t"+body
  # convert to string in registry format
  def reg = "#{feed.nick}\t#{feed.url}\t#{self.to_s}"
  def <=>(other) = self.timestamp <=> other.timestamp
  def nick = (if self.feed then self.feed.nick end) or ""
  def Post.parse(feed, raw)
    parts = raw.split("\t", 2)
    return Post.new feed, DateTime.parse(parts[0]), parts[1]
  end
end

# superclass for all feeds
class Feed
  def meta = Hash.new
  def posts = []
  def refresh = nil
  def nick = nil
  def url = nil
  def render(max)
    max = max.clamp(0, self.posts.length)
    for post in self.posts[-max..-1] do
      # someone thought it would be a good idea for ruby to try to handle transcoding automatically.  ignore all that and assume everything is utf-8.
      print ("<#{post.feed.nick}> #{post.body}\n").force_encoding('utf-8')
    end
    return nil
  end
  def lookup_user(nick)
    posts.each{|p|
      if p.nick == nick then
        return p.feed
      end
    }
    return nil
  end
  def follows = []
end

class MultiFeed < Feed
  def initialize(feeds)
    @feeds = feeds
  end
  def posts
    l = []
    for subfeed in @feeds do
      l = l.concat subfeed.posts
    end
    return l.sort
  end
  def refresh
    for subfeed in @feeds do
      p "refreshing #{subfeed.url}"
      subfeed.refresh 
    end
    return nil
  end
  def nearby
    l = []
    @feeds.each{|f|
      f.refresh
      #p f
      l = l + f.follows
    }
    return MultiFeed.new l
  end
end

class PagedFeed < Feed
  "feed that consists of several pages, like a twtxt registry"
  def initialize(url)
    @url = url
    @posts = []
  end
  def refresh
    # TODO: combine with posts already in the feed?
    @posts = get_page(1) + get_page(2) + get_page(3)
    return nil
  end
  def url_with_arg(k, v)
    c = '?'
    if @url.to_s.chars.member? '?' then
      r = r + '&'
    end
    return "#{@url}#{c}#{k}=#{v}"
  end
  def get_page(n)
    page = []
    fetch_url(url_with_arg "page", n.to_s).split("\n").each{|ln|
      nick, u, ts, msg = ln.split("\t", 4)
      post = Post.new(UserFeed.ref(nick, u), ts, msg)
      page << post
    }
    return page
  end
  attr_reader :posts
end
