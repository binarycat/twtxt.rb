require './feed.rb'

class Registry
  def initialize(u)
    if u[-1] != '/' then
      u << '/'
    end
    @baseurl = URI(u)
  end
  def api_plain(s) = @baseurl+'plain/'+s
  def tag(tagname)
    return PagedFeed.new(@baseurl+'plain/tags/'+tagname)
  end
  def latest = PagedFeed.new(api_plain 'tweets')
  def users = PagedFeed.new(api_plain 'users')
  def lookup_user(u)
    begin
      nick, url = fetch_url(api_plain "users?q=#{u}").split("\t")
      return UserFeed.ref(nick, url)
    rescue
      return nil
    end
  end
end
