require './userfeed.rb'

class Client
  def initialize(filepath)
    @my_feed = FileFeed.new(filepath)
    @current_pos = 0
    @current_len = 10
    @current_feed = @my_feed
  end
  def new_post(body)
    @my_feed.new_post body
  end
  def goto_newest
    @current_feed.refresh
    @current_pos = 0
  end
  def goto_next
    @current_pos += @current_len
  end
  def goto_prev = @current_pos -= @current_len
  def goto_feed(feed)
    if feed == nil then return nil end
    @current_feed = feed
    self.goto_newest
  end
  def goto_timeline = goto_feed @my_feed.timeline
  def goto_registry = goto_feed @my_feed.registry.latest
  def goto_user(u)
    if u.match? /:\/\// then
      goto_feed 'UNKNOWN', u
    elsif user = @my_feed.lookup_user(u) then
      goto_feed user
    elsif user = @current_feed.lookup_user(u) then
      goto_feed user
    elsif @my_feed.registry and (user = @my_feed.registry.lookup_user(u)) then
      goto_feed user
    else
      print "unknown user: #{u}\n"
    end
  end
  def current
    z = @current_pos + @current_len + 1
    l = @current_feed.posts.length
    z = z.clamp(0, l)
    a = z - @current_len - 1
    a = a.clamp(0, l)
    return @current_feed.posts.sort.reverse[a..z]
  end
end

# repl interface loosly based on the unix mail program
class TermClient < Client
  def eval_line(s)
    arg = s[1..-1].strip
    if s[0] == 'n' then
      self.goto_next
    elsif s[0] == 'p' then
      self.goto_prev
    elsif s[0] == 'r' then
      self.goto_newest
    elsif s[0] == 't' then
      self.goto_timeline
    elsif s[0] == 's' then
      self.show
    elsif s[0] == 'u' || s[0] == '@' then
      # set the current feed to a user feed
      arg = s[1..-1].strip
      n = arg.to_i
      # if passed an integer, go to the user feed associated with that number post
      if n.to_s == arg then
        goto_feed self.current[n].feed
      else
        goto_user arg
      end
    elsif s[0] == '+' then
      arg = s[1..-1].strip
      @my_feed.new_post arg
    elsif s[0] == 'q' then
      @eval_loop = false
    elsif s[0] == 'g' then # global feed 
      if @my_feed.registry == nil then
        print "no 'registry=' metadata key in user feed\n"
      else
        goto_registry
      end
    elsif s[0] == 'x' then # eXtended feed
      goto_feed @my_feed.timeline.nearby
    elsif s[0] == 'm' then # add metadata key
      k, v = arg.split(' ', 2)
      @my_feed.refresh
      @my_feed.add_meta k, v
      @my_feed.flush
    else
      # TODO: h = help command
      print "unknown command\n"
    end
    return nil
  end
  def eval_stream(io = STDIN)
    @eval_loop = true
    begin
      while @eval_loop do
        print '> '
        eval_line(io.readline)
      end
    rescue EOFError
      return nil
    end
  end
  def show
    self.current.each_with_index{|p, i|
      print "#{i}. <#{p.nick}> #{p.body}\n"
    }
  end
end
