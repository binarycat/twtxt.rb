require './client.rb'

twtxt_file = '/home/binarycat/mnt/envs/public_html/twtxt.txt'

if not File.exists? twtxt_file then
  `sshfs binarycat@envs.net: ~/mnt/envs`
end

cli = TermClient.new  twtxt_file
cli.eval_stream
